#!/C/Python33/python
import socket
import sys

s = socket.socket()
port = 12345
host = sys.argv[1]
print('Connecting to', host)

s.connect((host, port))
while True:
  msg = s.recv(1024)
  print(str(msg, 'UTF8'))
  msg = input()
  s.send(bytearray(msg, 'UTF8'))
s.close()
