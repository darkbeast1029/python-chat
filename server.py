#!/C/Python33/python
import socket

s = socket.socket()
port = 12345
s.bind(('0.0.0.0', port))

s.listen(5)
while True:
  c, addr = s.accept()
  print('Messaging', addr)
  c.send(b'Thank you for messaging!')
  while True:
    msg = c.recv(1024)
    print(str(msg, 'UTF8'))
    msg = input()
    c.send(bytearray(msg, 'UTF8'))
  c.close()
s.close()
